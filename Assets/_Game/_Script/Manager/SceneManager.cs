﻿using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;
using System.Text;
using System.Collections;
using System.Collections.Generic;
using TMPro;
using System;
using DG.Tweening;

public class SceneManager : MonoBehaviour {
    public static SceneManager ins;
    public static FormUI formLast;
    public AsyncOperation async;
   
    [Header("--------------- Trạng thái của Scene ---------------")]
    bool isNoWait_RemoteConfig = false;//Đợi RemoveConfig lâu quá thì vào game luôn (Giá trị RemoveConfig trả về muộn thì vẫn sẽ nhận đc)
    [SerializeField] public bool isChangingForm = false; 

    [Header("--------------- Pause ---------------")]
    public bool isPause = false;
    [SerializeField] private bool isPauseF5 = false;
    [SerializeField] public bool isPauseSound = false;
    [SerializeField] private float timeBlock = 0;//Block Input theo thời gian
    [SerializeField] private bool isBlockInput;//Block Input theo sự kiện
    public GameObject obj_BlockInput;

    [Header("--------------- Popup đã đc tạo trong Scene ---------------")]
    [HideInInspector] public Popup_EndGame popup_EndGame;
    [HideInInspector] public Popup_Revive popup_Revive;
    [HideInInspector] public Popup_Tutorial popup_Tutorial;
    [HideInInspector] public Popup_Story popup_Story;
    [HideInInspector] public Popup_Settings popup_Settings;

    [Header("--------------- UI trong Scene ---------------")]
    public Form_Loading form_Loading;
    public Form_Gameplay formGameplay;
    public Form_Home formHome;
    public Canvas formCanvas;
    public Canvas popupCanvas;
    public Canvas loadingCanvas;
    public Canvas canvas_Indicator;
    public Canvas canvas_Joystick;
    public FormBase formCurrent;

    public GameObject obj_Gold;
    public TextMeshProUGUI txt_UIMoney;
    public Coroutine coroutineTweenGem;
    [SerializeField] Action call_back_tween_gem;
    //public List<Data_Intelligent> intelligentList;
    public Image iconGold;
    public TextMeshProUGUI obj_CupTxt;
    public GameObject obj_Cup;
    #region Main
    private void Awake() {
        if (ins != null) Destroy(ins.gameObject);
        ins = this;
    }

    protected void Start() {
        if (GameManager.ins == null) return;
        isChangingForm = false;
        if (GameManager.ins.isFormLoading_Proccess) {
            formCurrent.Show(); 
        }
        if (txt_UIMoney != null) txt_UIMoney.text = DataManager.ins.gameSave.gold + "";
        if (obj_CupTxt != null) obj_CupTxt.text = DataManager.ins.gameSave.amountWinAll.ToString();
    }

    void Update() {
        if (async != null) return;
        //Rung nhẹ mỗi khi nhấn vào màn hình
        //if (Input.GetMouseButtonDown(0) && DataManager.ins != null && DataManager.ins.gameSave.isVibrate) MoreMountains.NiceVibrations.MMVibrationManager.Vibrate();
        //Pause game
#if UNITY_EDITOR
        if (Input.GetKeyDown(KeyCode.F5)) isPauseF5 = !isPauseF5;
#endif
        if (GameManager.ins != null) { 
            Time.timeScale = (isPause || isPauseF5) ? 0 : 1;
            AudioListener.pause = (isPauseSound);
        }
        //User bấm nút Back trên điện thoại
        /*if (Input.GetKeyDown(KeyCode.Escape) && !obj_BlockInput.activeSelf)
        {
            //Đóng các Popup lại
            PopupBase[] listPopup =  popupCanvas.GetComponentsInChildren<PopupBase>();
            if (listPopup.Length > 0)
            {
                //Nếu popup cuối cùng (Ở trên cùng) có thể tắt đc -> Tắt popup
                if (listPopup[listPopup.Length - 1].isCloseByEscape)
                {
                    listPopup[listPopup.Length - 1].Close();
                }
            }
        }*/
    }
    #endregion


    #region Block Input
    public void BlockInput(bool isBlock) {//BlockInput theo sự kiện
        isBlockInput = isBlock;
        if ((timeBlock <= 0 && !isBlock) || isBlock) obj_BlockInput.SetActive(isBlock);
    }

    public void BlockInput(float second) {//BlockInput theo thời gian
        obj_BlockInput.SetActive(true);
        if (second > timeBlock) timeBlock = second;
        StartCoroutine(CountTimeBlockInput());
    }

    private IEnumerator CountTimeBlockInput() {
        yield return new WaitForSeconds(0.5f);
        timeBlock -= 0.5f;
        if (timeBlock <= 0) {
            timeBlock = 0;
            if (!isBlockInput) obj_BlockInput.SetActive(false);//Nếu ko block theo even thì mới mở Block
        } else {
            StartCoroutine(CountTimeBlockInput());//Tiếp tục đếm thời gian Block Input
        }
    }
    #endregion

    #region Tween Money
    private IEnumerator TweenGem(int gem, Action _call_back_tween_gem = null) {
        if (_call_back_tween_gem != null) {
            this.call_back_tween_gem = _call_back_tween_gem;
        }
        if (txt_UIMoney != null) {
            if (gem > 0) {
                int tweenGem = int.Parse(txt_UIMoney.text);
                int amount = (gem <= 50 ? 2 : gem < 100 ? 4 : gem < 200 ? 6 : gem < 300 ? 8 : gem < 400 ? 8 : gem < 500 ? 10 : gem < 800 ? 20 : gem < 1600 ? 40 : gem / 30);
                while (DataManager.ins.gameSave.gold > tweenGem) {
                    tweenGem += amount;
                    txt_UIMoney.text = tweenGem.ToString();
                    txt_UIMoney.color = new Color(0.5f, 1, 0.5f);

                    yield return new WaitForSeconds(0.02f);
                }
                txt_UIMoney.text = DataManager.ins.gameSave.gold.ToString();
            } else {
                int tweenGem = int.Parse(txt_UIMoney.text);
                int amount = (gem > -30 ? -2 : gem > -100 ? -5 : gem > -200 ? -10 : gem > -800 ? -20 : gem > -1600 ? -40 : gem / 20);
                if (DataManager.ins.gameSave.gold < tweenGem) {
                    while (DataManager.ins.gameSave.gold < tweenGem) {
                        tweenGem += amount;
                        txt_UIMoney.text = tweenGem.ToString();
                        txt_UIMoney.color = new Color(1,0.5f, 0.5f);

                        yield return new WaitForSeconds(0.02f);
                    }
                } else {
                    while (DataManager.ins.gameSave.gold > tweenGem) {
                        tweenGem -= amount;
                        txt_UIMoney.text = tweenGem.ToString();
                        txt_UIMoney.color =  new Color(0.5f, 1, 0.5f);

                        yield return new WaitForSeconds(0.02f);
                    }
                }

                txt_UIMoney.text = DataManager.ins.gameSave.gold.ToString();
            }
            txt_UIMoney.color = Color.white;

        }

        yield return new WaitForSeconds(0.3f);


        if (this.call_back_tween_gem != null) {
            this.call_back_tween_gem();
            this.call_back_tween_gem = null;
        }
        coroutineTweenGem = null;
    }


    public void OpenTweenGem(TextMeshProUGUI txt_gem,int currentGold = 0,Action _call_back_tween_gem = null) {
        txt_UIMoney = txt_gem;
        if (coroutineTweenGem != null) {
            StopCoroutine(coroutineTweenGem);
            txt_UIMoney.text = currentGold.ToString();
        }

        coroutineTweenGem = StartCoroutine(TweenGem(DataManager.ins.gameSave.gold - int.Parse(txt_UIMoney.text), _call_back_tween_gem));
    }

    #endregion

    #region Form
    //Chuyển sang 1 Form mới
    public void ChangeForm(string formNext, float time = 0) {
        if (isChangingForm) return;
        isChangingForm = true;
        formLast = formCurrent.idForm;
        BlockInput(time + 0.5f);
        Timer.Schedule(this, time, () => { 
            async = UnityEngine.SceneManagement.SceneManager.LoadSceneAsync(formNext); 
        });
    }


    #endregion

    #region Popup
    public void ShowPopup_Settings() {
        if (popup_Settings == null) {
            popup_Settings = Instantiate(GameManager.ins.popup_Settings, popupCanvas.transform);
        }

        popup_Settings.transform.localScale = Vector3.one;
        popup_Settings.transform.SetAsLastSibling();
        popup_Settings.gameObject.SetActive(false);
        popup_Settings.Show();

    }

    public void ShowPopup_Tutorial() {
        if (popup_Tutorial == null) {
            popup_Tutorial = Instantiate(GameManager.ins.popup_Tutorial, popupCanvas.transform);
        }
        popup_Tutorial.transform.localScale = Vector3.one;
        popup_Tutorial.transform.SetAsLastSibling();
        popup_Tutorial.gameObject.SetActive(false);
        popup_Tutorial.Show();
    }
    public void ShowPopup_Story() {
        if (popup_Story == null) {
            popup_Story = Instantiate(GameManager.ins.popup_Story, popupCanvas.transform);
        }
        popup_Story.transform.localScale = Vector3.one;
        popup_Story.transform.SetAsLastSibling();
        popup_Story.gameObject.SetActive(false);
        popup_Story.Show();
    }

    public void ShowPopup_Revive() {
        if (popup_Revive == null) {
            popup_Revive = Instantiate(GameManager.ins.popup_Revive, popupCanvas.transform);
        }
        popup_Revive.transform.localScale = Vector3.one;
        popup_Revive.transform.SetAsLastSibling();
        popup_Revive.gameObject.SetActive(false);
        popup_Revive.Show();
    }

    public void ShowPopup_EndGame(bool isWin = false, float timeDelay = 0) {
        if (popup_EndGame == null) {
            popup_EndGame = Instantiate(GameManager.ins.popup_EndGame, popupCanvas.transform);
        } else if (popup_EndGame.gameObject.activeSelf) {
            return;
        }
        popup_EndGame.isWin = isWin;
        popup_EndGame.transform.localScale = Vector3.one;
        popup_EndGame.transform.SetAsLastSibling();
        popup_EndGame.gameObject.SetActive(false);
        Timer.Schedule(this, timeDelay, () => {
            popup_EndGame.Show();
        });
    }

    #endregion
    /*
    public bool IsOpenPopUp() {
        //check xem dang mo UI nao
        if (popupList.Count < 0) return false;
        for (int i = 0; i < popupList.Count; i++) {
            if (popupList [i].gameObject.activeSelf) {
                return true;
            }
        }
        return false;
    }*/
}


﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GameManager : Singleton<GameManager> {
    [Header("--------------- Cheat & Hack --------")]
    public int rankMinigame = -1;
    public int mapMinigame = -1;
    //public bool isABTest = false;
    public bool isPlayMinigameNow = false;
    public bool isRemoveAllAds = false;
    public bool isEnableCheat = false;//Bật cheat trong Setting: No Banner, No Inter + PopupSetting: Hide UIGameplay, cheat Gold, cheat Levels 
    
    [Header("--------------- Trạng thái đặc biệt --------")]
    public bool isIOS = false;
    public bool isFormLoading_Proccess = false;
    public bool isSelectedChar = false;
    public bool isHideUIGameplay = false;
    public bool isShowOfflineEarning = false;

    [Header("--------------- Prefab của Popup --------")]
    public Popup_EndGame popup_EndGame;
    public Popup_Revive popup_Revive;
    public Popup_Tutorial popup_Tutorial;
    public Popup_Story popup_Story;
    public Popup_Settings popup_Settings;

    [Header("-------- List Data --------")]
    public PrefabLevel[] listLevel;

    [Header("--------------- Other --------")]
    public GameObject obj_SplashScreen;

    [Header("------Time---------")]
    public int timeOffer = 999;
   

    #region Unity
    public override void Awake() {
        base.Awake();
        isIOS = (Application.platform == RuntimePlatform.IPhonePlayer || Application.platform == RuntimePlatform.OSXPlayer || Application.platform == RuntimePlatform.OSXEditor);
        QualitySettings.vSyncCount = 0;
        Application.targetFrameRate = Constants.FRAME_RATE;
        //Đếm thời gian chơi
        StartCoroutine(CountTime());
    }

    IEnumerator CountTime()
    {
        while (true)
        {
            yield return new WaitForSecondsRealtime(1f);
            timeOffer++;
        }
    }


    IEnumerator Start() {
        if (isRemoveAllAds == false) {
            //UI Splash screen
            DataManager.ins.LoadData();
            isFormLoading_Proccess = true;
            SoundManager.ins.ReloadMusic();
            yield return new WaitUntil(() => SceneManager.ins.form_Loading.fillAmountProcess > 1.1f);
            SceneManager.ins.formCurrent.Show();
        } else {
            //UI Splash screen
            DataManager.ins.LoadData();
            SceneManager.ins.formCurrent.Show();
            isFormLoading_Proccess = true;
            SoundManager.ins.ReloadMusic();
        }
        DataManager.ins.SaveGame();
        if (DataManager.ins.gameSave.totalSession == 0) {
            /*
            if (DataManager.ins != null && DataManager.ins.gameSave != null) {
                    DataManager.ins.gameSave.difficulty = PluginManager.ins.DifficultyStart;
            } else {
                Debug.LogError("Lỗi ko thay đổi đc DifficultyStart: Do Data chưa đc load lên");
            }*/
            SceneManager.ins.ChangeForm(FormUI.Form_Home.ToString(), 0);
        } else {
            SceneManager.ins.ChangeForm(FormUI.Form_Home.ToString(), 0);
        }
    }
    /*
    void OnDestroy() {
        if (!Debug.isDebugBuild && !Application.isEditor) {
            //số level đã chơi(số level đã start -mỗi level chỉ bắn 1 lần)
            FirebaseManager.ins.OnSetProperty("level_played", DataManager.ins.gameSave.levelWin);
        }
    }*/
    #endregion

}

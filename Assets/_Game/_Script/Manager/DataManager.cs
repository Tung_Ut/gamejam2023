﻿using System;
using System.Collections.Generic;
using UnityEngine;

public class DataManager : Singleton<DataManager> {
    public bool isLoaded = false;

    public GameSave gameSave;
    private GameSave gameSave_BackUp;//Luôn giống Data gốc, nhưng sẽ check để ko thể bị lỗi

    public List<int>[] list_IDFull_ByColor;//Danh sách các skin full theo màu char

    #region Unity
    private void OnApplicationPause(bool pause) { SaveGame(); }
    private void OnApplicationQuit() { SaveGame(); }
    #endregion

    public void LoadData() {
        try {
            if (isLoaded == false) {
                if (PlayerPrefs.HasKey("GameSave")) gameSave = JsonUtility.FromJson<GameSave>(PlayerPrefs.GetString("GameSave"));
                if (gameSave.isNew) {
                    gameSave = new GameSave();
                    gameSave.isNew = false;
                } else {
                    gameSave.totalSession++;
                    int timeNow = (int)DateTime.Now.Subtract(new DateTime(1970, 1, 1)).TotalDays;
                    if (timeNow - gameSave.timeLastOpen > 0) {//Nếu sang ngày mới thì ??
                        gameSave.daysPlayed++;
                        gameSave.totalDays = timeNow - gameSave.timeInstall;
                        GameManager.ins.isShowOfflineEarning = true;
                    }
                    gameSave.timeLastOpen = timeNow;
                }
                //Cheat
                if (GameManager.ins.isRemoveAllAds) gameSave.isNoAds = true;


                isLoaded = true;
                SaveGame();
            }
        } catch (Exception ex) {
            Debug.LogError("Lỗi LoadData:" + ex);
        }
    }

    public void SaveGame() {
        try {
            if (!isLoaded) return;

            if (gameSave == null) {
                if (gameSave_BackUp != null) {
                    gameSave = gameSave_BackUp;
                    Debug.LogError("gameSave bị null, backup thành công");
                } else {
                    gameSave = new GameSave();
                    Debug.LogError("gameSave bị null, backup ko thành công. Reset data");
                }
            }
            gameSave_BackUp = gameSave;

            PlayerPrefs.SetString("GameSave", JsonUtility.ToJson(gameSave));
            PlayerPrefs.Save();
        } catch (Exception ex) {
            Debug.LogError("Lỗi LoadData:" + ex);
        }
    }

    public void ChangeGold(int amount, string nameEvent, bool isFirebase = true, bool isReloadTextGold = true) {
        if (amount == 0) return;
        gameSave.gold += amount;
        if (SceneManager.ins.txt_UIMoney != null && isReloadTextGold) SceneManager.ins.txt_UIMoney.text = gameSave.gold + "";
        if (isFirebase) {
            if (amount > 0) {
            } else {
            }
        }
        SaveGame();
    }

    

}

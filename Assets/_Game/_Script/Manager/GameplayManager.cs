﻿using DG.Tweening;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class GameplayManager : Singleton<GameplayManager>
{
    public bool isStartGame = false;
    public int stageCur = 0;
    public int amountRootCur = 0;
    public int amountReveive = 0;
    public Image[] listTrees;
    public List<EarthCell> listCellH2O = new List<EarthCell>();
    public EarthCell[,] listAllEarthCell;
    public List<EarthCell> listMyEarthCell = new List<EarthCell>();
    public PrefabLevel prefabLevel;
    public EarthCell earthCellSelected;
    public NodeH2O nodeH2OBase;
    
    public Transform tranParentTree;
    public Transform tranCanvas;

    /*
    public bool isValidate = false;
    public EarthCell earthCellBase;
    public Transform earthCellParent;
    public PrefabLevel prefabLevelNew;
    public void OnValidate()
    {
        if (isValidate)
        {
            prefabLevelNew.listAllEarthCell.Clear();
            listAllEarthCell = new EarthCell[prefabLevelNew.MaxSizeX, prefabLevelNew.MaxSizeY];
            for (int y = 0; y < prefabLevelNew.MaxSizeX; y++)
            {
                for (int x = 0; x < prefabLevelNew.MaxSizeY; x++)
                {
                    listAllEarthCell[x, y] = Instantiate<EarthCell>(earthCellBase, earthCellParent);
                    listAllEarthCell[x, y].name = "EarthCell (" + x + "," + y + ")";
                    listAllEarthCell[x, y].x = x;
                    listAllEarthCell[x, y].y = y;
                    prefabLevelNew.listAllEarthCell.Add(listAllEarthCell[x, y]);
                }
            }
            //if(prefabLevelOld != null)
            //{
            //    for (int i = 0; i < prefabLevelOld.listAllEarthCell.Count; i++)
            //    {
            //        prefabLevelOld.listAllEarthCell[i].type = prefabLevel.listAllEarthCell[i].type;
            //        prefabLevelOld.listAllEarthCell[i].button.image.color = prefabLevel.listAllEarthCell[i].transform.GetChild(0).GetComponent<Image>().color;
            //    }
            //}

        }
    }*/

    public void Init()
    {
        stageCur = 0;
        prefabLevel = Instantiate<PrefabLevel>(GameManager.ins.listLevel[Mathf.Clamp(DataManager.ins.gameSave.level, 0, GameManager.ins.listLevel.Length - 1)], transform);
        listAllEarthCell = new EarthCell[prefabLevel.MaxSizeX, prefabLevel.MaxSizeY];
        amountRootCur = prefabLevel.MaxGrow[stageCur];
        CameraControl.ins.ShrinkView();
        //Cây phát triển to lên -> Bắt đầu stage mới
        for (int i = 0; i < listTrees.Length; i++) {
            listTrees[i].sprite = prefabLevel.listSpriteTree[i];
            listTrees[i].gameObject.SetActive(i == stageCur);
        }
        SceneManager.ins.formHome.img_BG.sprite = prefabLevel.spriteBG;
        SceneManager.ins.formHome.ReloadText();
        //Vào game thì sẽ khởi tạo map 9x9
        for (int y = 0; y < prefabLevel.MaxSizeY; y++)
        {
            for (int x = 0; x < prefabLevel.MaxSizeX; x++)
            {
                listAllEarthCell[x, y] = prefabLevel.listAllEarthCell[x+y* prefabLevel.MaxSizeX];
                listAllEarthCell[x, y].name = "EarthCell ("+x+","+y+")";
                listAllEarthCell[x, y].x = x;
                listAllEarthCell[x, y].y = y;
            }
        }
    }

    public void StartGame() {
        GameplayManager.ins.isStartGame = true;

        //Đặt Root tại vị trí giữa trên
        listAllEarthCell[prefabLevel.MaxSizeX / 2, 0].imgRoot.gameObject.SetActive(true);
        listAllEarthCell[prefabLevel.MaxSizeX / 2, 0].objLockMap.gameObject.SetActive(false);
        listMyEarthCell.Add(listAllEarthCell[prefabLevel.MaxSizeX / 2, 0]);
        //Hiển thị các cell có thể Grow đc từ Root!
        ClickToCell(listAllEarthCell[prefabLevel.MaxSizeX / 2, 0], true);
    }

    public void CheckWin()
    {
        //Nếu đã đủ nước rồi thì chuyển stage luôn
        if (listCellH2O.Count >= prefabLevel.MaxMissionH2O[stageCur]) {//Nếu hoàn thành điều kiện
                                                           //Chuyển Stage
            StartCoroutine(ChangeStage(stageCur + 1));
        } else {
            if (amountRootCur <= 0) {//Nếu hết Root -> Thua
                SceneManager.ins.ShowPopup_Revive();
            }
        }
    }
    IEnumerator ChangeStage(int stageNext)
    {
        stageCur = stageNext;
        //Tạo ra các cục H2O ở các Cell có nước
        List<NodeH2O> listH2OFly = new List<NodeH2O>();
        for (int i = 0; i < listCellH2O.Count; i++)
        {
            NodeH2O nodeH2O = Instantiate<NodeH2O>(nodeH2OBase, tranCanvas);
            nodeH2O.transform.position = listCellH2O[i].transform.position;
            nodeH2O.gameObject.SetActive(true);
            nodeH2O.cellStart = listCellH2O[i];
            listH2OFly.Add(nodeH2O);
        }
        SceneManager.ins.BlockInput(true);
        CameraControl.ins.ZoomView();
        yield return new WaitForSeconds(1f);
        //Bay nước đến cây
        int amountFlyMax = 0;
        for (int i = 0; i < listH2OFly.Count; i++)
        {  //Tìm số lần đi nhiều nhất của H2O
            if (listH2OFly[i].cellStart.pathToRoot.Count > amountFlyMax) amountFlyMax = listH2OFly[i].cellStart.pathToRoot.Count;
        }
        //Duyệt theo số lần đi nhiều nhất
        for (int i = 0; i < amountFlyMax; i++)
        {//Duyệt tất cả những H2O còn số lần đi
            for (int j = 0; j < listH2OFly.Count; j++)
            {
                //Nếu còn số lần đi thì DOMove
                if (listH2OFly[j].cellStart.pathToRoot.Count > i) {
                    listH2OFly[j].transform.DOMove(listH2OFly[j].cellStart.pathToRoot[i].transform.position, 0.4f);
                }
                else {
                    if (listH2OFly[j].objImg.activeSelf) listH2OFly[j].soundObj.PlaySound();
                        listH2OFly[j].objImg.SetActive(false);
                }
            }
            yield return new WaitForSeconds(0.2f);
        }
        //Tắt node H2O đi
        for (int i = 0; i < listH2OFly.Count; i++)
        {
            if (listH2OFly[i].objImg.activeSelf) listH2OFly[i].soundObj.PlaySound();
            listH2OFly[i].objImg.SetActive(false);
        }
        CameraControl.ins.ShrinkView();
        SceneManager.ins.BlockInput(false);
        //Hiển thị cây phát triển to lên
        for (int i = 0; i < listTrees.Length; i++)
        {
            listTrees[i].gameObject.SetActive(i == stageCur);
        }

        if (stageCur < prefabLevel.MaxGrow.Length)
        {//Chuyển sang stage mới
            amountRootCur += prefabLevel.MaxGrow[stageCur];
            SceneManager.ins.formHome.ReloadText();
            SceneManager.ins.formHome.ShowAddRoot(prefabLevel.MaxGrow[stageCur]);
        }
        else
        {//Win game
            SceneManager.ins.BlockInput(1);
            SceneManager.ins.ShowPopup_EndGame(true, 1);
        }
    }

    public void ClickToCell(EarthCell cellClick, bool isBtnStartGame = false)
    {
        if (isStartGame == false && isBtnStartGame == false) return;
        //Nếu bấm vào cell xanh thì mọc rễ đến cell đó
        if (cellClick.boderGrow.activeSelf)
        {
            amountRootCur--;
            if (earthCellSelected.x - 1 == cellClick.x)
            {//L
                earthCellSelected.imgConnect_L.gameObject.SetActive(true);
                cellClick.imgConnect_R.gameObject.SetActive(true);
            }
            else if (earthCellSelected.x + 1 == cellClick.x)
            {//R
                earthCellSelected.imgConnect_R.gameObject.SetActive(true);
                cellClick.imgConnect_L.gameObject.SetActive(true);
            }
            else if (earthCellSelected.y - 1 == cellClick.y)
            {//T
                earthCellSelected.imgConnect_T.gameObject.SetActive(true);
                cellClick.imgConnect_B.gameObject.SetActive(true);
            }
            else if (earthCellSelected.y + 1 == cellClick.y)
            {//B
                earthCellSelected.imgConnect_B.gameObject.SetActive(true);
                cellClick.imgConnect_T.gameObject.SetActive(true);
            }//Unlock Map
            //Check các cell xung quanh
            if (cellClick.x - 1 >= 0)
            {//Nếu có ít nhất 1 ô bên trái
                listAllEarthCell[cellClick.x - 1, cellClick.y].objLockMap.SetActive(false);
            }
            if (cellClick.x + 1 < prefabLevel.MaxSizeX)
            {//Nếu có ít nhất 1 ô bên phải
                listAllEarthCell[cellClick.x + 1, cellClick.y].objLockMap.SetActive(false);
            }
            if (cellClick.y - 1 >= 0)
            {//Nếu có ít nhất 1 ô bên trên
                listAllEarthCell[cellClick.x, cellClick.y - 1].objLockMap.SetActive(false);
            }
            if (cellClick.y + 1 < prefabLevel.MaxSizeY)
            {//Nếu có ít nhất 1 ô bên dưới
                listAllEarthCell[cellClick.x, cellClick.y + 1].objLockMap.SetActive(false);
            }
            //
            earthCellSelected.listConnected.Add(cellClick);
            cellClick.listConnected.Add(earthCellSelected);
            cellClick.step = listMyEarthCell.Count;
            if (cellClick.type == TypeCell.H2O)
            {
                SoundManager.ins.sound_Click.PlaySound(2);
                listCellH2O.Add(cellClick);
                //Tìm con đường trở về gốc cây
                cellClick.pathToRoot.Add(cellClick);//Thêm bản thân
                cellClick.pathToRoot.Add(earthCellSelected);//Thêm cell vừa nối vào mình
                cellClick.pathToRoot = FindPathToRoot(cellClick.pathToRoot);
                SceneManager.ins.formHome.ShowSubRoot(cellClick, 1, true);
            } else {
                SceneManager.ins.formHome.ShowSubRoot(cellClick, 1);

            }
            earthCellSelected = cellClick;
            listMyEarthCell.Add(cellClick);
            //Hủy hiệu ứng chọn của tất cả cell cũ
            for (int x = 0; x < prefabLevel.MaxSizeX; x++)
            {
                for (int y = 0; y < prefabLevel.MaxSizeY; y++)
                {
                    listAllEarthCell[x, y].boderSelected.SetActive(false);
                    listAllEarthCell[x, y].boderGrow.SetActive(false);
                    listAllEarthCell[x, y].boderNoGrow.SetActive(false);
                }
            }
            SceneManager.ins.formHome.ReloadText();
            CheckWin();
            return;
        }

        //Hủy hiệu ứng chọn của tất cả cell cũ
        for (int x = 0; x < prefabLevel.MaxSizeX; x++)
        {
            for (int y = 0; y < prefabLevel.MaxSizeY; y++)
            {
                listAllEarthCell[x, y].boderSelected.SetActive(false);
                listAllEarthCell[x, y].boderGrow.SetActive(false);
                listAllEarthCell[x, y].boderNoGrow.SetActive(false);
            }
        }

        //Hiệu ứng chọn Cell mới
        earthCellSelected = cellClick;
        earthCellSelected.boderSelected.SetActive(true);
        earthCellSelected.boderGrow.SetActive(false);
        earthCellSelected.boderNoGrow.SetActive(false);
        if (listMyEarthCell.Contains(cellClick))
        {//Nếu ô đó có rễ thì mới grow đc xung quanh
            //Check các cell xung quanh
            if (cellClick.x - 1 >= 0)
            {//Nếu có ít nhất 1 ô bên trái
                listAllEarthCell[cellClick.x - 1, cellClick.y].objLockMap.SetActive(false);
                listAllEarthCell[cellClick.x - 1, cellClick.y].boderNoGrow.SetActive(
                    listMyEarthCell.Contains(listAllEarthCell[cellClick.x - 1, cellClick.y])
                    || cellClick.imgConnect_L.gameObject.activeSelf
                    || listAllEarthCell[cellClick.x - 1, cellClick.y].type == TypeCell.Block);
                listAllEarthCell[cellClick.x - 1, cellClick.y].boderGrow.SetActive(
                    !listAllEarthCell[cellClick.x - 1, cellClick.y].boderNoGrow.activeSelf);
            }
            if (cellClick.x + 1 < prefabLevel.MaxSizeX)
            {//Nếu có ít nhất 1 ô bên phải
                listAllEarthCell[cellClick.x + 1, cellClick.y].objLockMap.SetActive(false);
                listAllEarthCell[cellClick.x + 1, cellClick.y].boderNoGrow.SetActive(
                   listMyEarthCell.Contains(listAllEarthCell[cellClick.x + 1, cellClick.y])
                    || cellClick.imgConnect_R.gameObject.activeSelf
                    || listAllEarthCell[cellClick.x + 1, cellClick.y].type == TypeCell.Block);
                listAllEarthCell[cellClick.x + 1, cellClick.y].boderGrow.SetActive(
                    !listAllEarthCell[cellClick.x + 1, cellClick.y].boderNoGrow.activeSelf);
            }
            if (cellClick.y - 1 >= 0)
            {//Nếu có ít nhất 1 ô bên trên
                listAllEarthCell[cellClick.x, cellClick.y - 1].objLockMap.SetActive(false);
                listAllEarthCell[cellClick.x, cellClick.y - 1].boderNoGrow.SetActive(
                    listMyEarthCell.Contains(listAllEarthCell[cellClick.x, cellClick.y - 1])
                    || cellClick.imgConnect_T.gameObject.activeSelf
                    || listAllEarthCell[cellClick.x, cellClick.y - 1].type == TypeCell.Block);
                listAllEarthCell[cellClick.x, cellClick.y - 1].boderGrow.SetActive(
                    !listAllEarthCell[cellClick.x, cellClick.y - 1].boderNoGrow.activeSelf);
            }
            if (cellClick.y + 1 < prefabLevel.MaxSizeY)
            {//Nếu có ít nhất 1 ô bên dưới
                listAllEarthCell[cellClick.x, cellClick.y + 1].objLockMap.SetActive(false);
                listAllEarthCell[cellClick.x, cellClick.y + 1].boderNoGrow.SetActive(
                    listMyEarthCell.Contains(listAllEarthCell[cellClick.x, cellClick.y + 1])
                    || cellClick.imgConnect_B.gameObject.activeSelf
                    || listAllEarthCell[cellClick.x, cellClick.y + 1].type == TypeCell.Block);
                listAllEarthCell[cellClick.x, cellClick.y + 1].boderGrow.SetActive(
                    !listAllEarthCell[cellClick.x, cellClick.y + 1].boderNoGrow.activeSelf);
            }
        }
        CheckWin();
    }

    public List<EarthCell> FindPathToRoot(List<EarthCell> listPathToRoot)
    {
        bool isDone = false;
        while (isDone == false)
        {
            Debug.LogWarning(listPathToRoot.Count + "_X: " + listPathToRoot[listPathToRoot.Count - 1].x +"_Y: " + listPathToRoot[listPathToRoot.Count - 1].y +"_Connect: " + listPathToRoot[listPathToRoot.Count - 1].listConnected.Count );
            if (listPathToRoot[listPathToRoot.Count - 1].listConnected.Count > 0)
            {
                EarthCell cellNext = null;
                for (int i = 0; i < listPathToRoot[listPathToRoot.Count - 1].listConnected.Count; i++)
                {//Tìm ra connect có step nhỏ nhất -> Chính là đường về gốc
                    if (cellNext == null || (cellNext.step > listPathToRoot[listPathToRoot.Count - 1].listConnected[i].step))
                    {
                        cellNext = listPathToRoot[listPathToRoot.Count - 1].listConnected[i];
                    }
                }
                if (cellNext.step > listPathToRoot[listPathToRoot.Count - 1].step) cellNext = null;
                if (cellNext != null)
                {
                    listPathToRoot.Add(cellNext);
                    //Tìm tiếp
                }
                else
                {
                    isDone = true;
                }
            }
            else
            {
                isDone = true;
            }
        }
        return listPathToRoot;
    }
}

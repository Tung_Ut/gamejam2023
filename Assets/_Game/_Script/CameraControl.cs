﻿using System;
using UnityEngine;
using DG.Tweening;

public class CameraControl : Singleton<CameraControl>
{
    Camera mainCamera;
    float x_input;
    float y_input;
    [SerializeField]
    float zoomLimit;
    [SerializeField]
    float zoomLimitNew;
    [SerializeField]
    float xLimit;
    [SerializeField]
    float yLimit;

    void Start()
    {
        mainCamera = GetComponent<Camera>();
    }

    void Update()
    {
        mainCamera.fieldOfView -= zoomLimit;
        if (Math.Abs(zoomLimit) >= 0.04f)
        {
            if (zoomLimit > 0)
            {
                zoomLimit -= 0.02f;
            }
            else
                zoomLimit += 0.02f;
        }
        else
        {
            zoomLimit = 0;
        }
        if (Input.GetMouseButtonDown(0) && GameplayManager.ins.isStartGame && !SceneManager.ins.obj_BlockInput.activeSelf)
        {
            x_input = Input.mousePosition.x;
            y_input = Input.mousePosition.y;
        }
        if (Input.GetMouseButton(0) && GameplayManager.ins.isStartGame && !SceneManager.ins.obj_BlockInput.activeSelf)
        {

            float x = -x_input + Input.mousePosition.x;
            float y = y_input - Input.mousePosition.y;


            mainCamera.transform.position += new Vector3(x, y, 0) * Time.deltaTime * 0.12f;
            mainCamera.transform.localPosition = new Vector3(Mathf.Clamp(mainCamera.transform.localPosition.x, -GameplayManager.ins.prefabLevel.MaxCameraX, GameplayManager.ins.prefabLevel.MaxCameraX), Mathf.Clamp(mainCamera.transform.localPosition.y, -GameplayManager.ins.prefabLevel.MaxCameraY, GameplayManager.ins.prefabLevel.MaxCameraY),0) ;
        }
        
    }

    public void ZoomView()
    {//Zoom ra
        zoomLimit = - zoomLimitNew;
        transform.DOMove(Vector3.one, 0.8f);
    }

    public void ShrinkView()
    {//Thu nhỏ
        zoomLimit = zoomLimitNew;
    }
}

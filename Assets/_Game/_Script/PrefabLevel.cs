﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PrefabLevel : MonoBehaviour
{
    public int MaxSizeX = 9;
    public int MaxSizeY = 9;
    public float MaxCameraX = 0.19f;
    public float MaxCameraY = 0.36f;
    public int[] MaxGrow = new int[5] { 8, 6, 6, 6, 6 };
    public int[] MaxMissionH2O = new int[5] { 2, 4, 6, 8, 10 };
    public List<EarthCell> listAllEarthCell;
    public Sprite spriteBG;
    public Sprite[] listSpriteTree;
}

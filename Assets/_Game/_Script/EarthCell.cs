﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

[System.Serializable]
public class EarthCell : MonoBehaviour
{
    public int x;
    public int y;
    public TypeCell type;
    public int step = 0;//Đc add vào root lúc có bao nhiêu phần tử
    public Button button;
    public GameObject boderSelected;
    public GameObject boderGrow;
    public GameObject boderNoGrow;
    public Image imgConnect_T;
    public Image imgConnect_B;
    public Image imgConnect_R;
    public Image imgConnect_L;
    public Image imgRoot;
    public GameObject objLockMap;
    public List<EarthCell> listConnected = new List<EarthCell>();
    public List<EarthCell> pathToRoot = new List<EarthCell>();

    //
    public void Btn_Click(EarthCell cellClick)
    {
        GameplayManager.ins.ClickToCell(cellClick);
    }

    private void Update()
    {
        
    }
}

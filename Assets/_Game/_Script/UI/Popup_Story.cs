﻿using DG.Tweening;
using System;
using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;
using UnityEngine.UI;

public class Popup_Story : PopupBase {

    public Sprite spriteOn;
    public Sprite spriteOff;
    public Image img_English;
    public Image img_Viet;
    public GameObject obj_English;
    public GameObject obj_Viet;

    public override void Show() {
        base.Show();
    }


    #region Button
    public void Btn_English() {
        SoundManager.ins.sound_Click.PlaySound();
        obj_English.gameObject.SetActive(true);
        obj_Viet.gameObject.SetActive(false);
        img_English.sprite = spriteOn;
        img_Viet.sprite = spriteOff;
    }
    public void Btn_Viet() {
        SoundManager.ins.sound_Click.PlaySound();
        obj_English.gameObject.SetActive(false);
        obj_Viet.gameObject.SetActive(true);
        img_English.sprite = spriteOff;
        img_Viet.sprite = spriteOn;
    }

    public void Btn_Close() {
        SoundManager.ins.sound_Click.PlaySound();
        Close();
    }
    #endregion
}


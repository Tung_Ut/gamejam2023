﻿using DG.Tweening;
using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;
using UnityEngine.UI;

public class Form_Home : FormBase {
    public TextMeshProUGUI txt_Level;
    public TextMeshProUGUI txt_Stage;
    public TextMeshProUGUI txt_Root;

    public TextMeshProUGUI txt_Mission;
    public Image img_Mission;
    public GameObject obj_Init;
    public GameObject obj_StartGame;

    public TextMeshProUGUI txt_AddRoots;
    public DOTweenAnimation twn_AddRoots;
    public TextMeshProUGUI txt_SubRoots;
    public TextMeshProUGUI txt_SubRootsBase;

    public Image img_BG;

    public override void Show()
    {
        base.Show();
        GameplayManager.ins.Init();
        
    }

    public void ReloadText()
    {
        txt_Level.text = "Level-" + (DataManager.ins.gameSave.level + 1);
        txt_Stage.text = "Stage " + (GameplayManager.ins.stageCur + 1) + "/5";
        txt_Root.text = "Root: " + GameplayManager.ins.amountRootCur;
        txt_Mission.text = GameplayManager.ins.listCellH2O.Count + "/" + GameplayManager.ins.prefabLevel.MaxMissionH2O[GameplayManager.ins.stageCur];
        img_Mission.fillAmount = (float)GameplayManager.ins.listCellH2O.Count / GameplayManager.ins.prefabLevel.MaxMissionH2O[GameplayManager.ins.stageCur];
    }

    public void ShowAddRoot(int amount) {
        txt_AddRoots.text = "+ " + amount + " Roots";
        txt_AddRoots.gameObject.SetActive(true);
        twn_AddRoots.DORestart();
    }

    public void ShowSubRoot(EarthCell cell ,int amount, bool isWater = false) {
        txt_SubRoots = Instantiate(txt_SubRootsBase, GameplayManager.ins.tranParentTree);
        txt_SubRoots.transform.position = cell.transform.position;
        txt_SubRoots.GetComponent<DOTweenAnimation>().endValueV3 = new Vector3(txt_SubRoots.transform.localPosition.x, txt_SubRoots.transform.localPosition.y + 40, txt_SubRoots.transform.localPosition.z);
        txt_SubRoots.text = "   - " + amount + " Root\n";
        if (isWater) txt_SubRoots.text += "   <color=blue>+ 1 Water source";
        txt_SubRoots.gameObject.SetActive(true);
    }

    #region Button
    public void Btn_Play() {
        SoundManager.ins.sound_Click.PlaySound();
        obj_Init.SetActive(false);
        obj_StartGame.SetActive(true);
        GameplayManager.ins.StartGame();
    }
    public void Btn_Tutorial() {
        SoundManager.ins.sound_Click.PlaySound();
        SceneManager.ins.ShowPopup_Tutorial();
    }

    public void Btn_Story() {
        SoundManager.ins.sound_Click.PlaySound();
        SceneManager.ins.ShowPopup_Story();
    }
    public void Btn_Setting() {
        SoundManager.ins.sound_Click.PlaySound();
        SceneManager.ins.ShowPopup_Settings();
    }
    #endregion
}
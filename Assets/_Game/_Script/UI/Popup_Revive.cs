﻿using DG.Tweening;
using System;
using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;
using UnityEngine.UI;

public class Popup_Revive : PopupBase {
    public int time;
    private int goldOld;
    private int costRevive;
    public TextMeshProUGUI txt_Time;
    public TextMeshProUGUI txt_Des;
    public DOTweenAnimation twn_BtnRevive;
    public DOTweenAnimation twn_Countdown;
    public SoundObject soundCountdown;
    public string[] listDes;

    public override void Show() {
        base.Show();
        time = 5;
        txt_Time.text = time + "";
        txt_Des.text = listDes[ Mathf.Clamp(GameplayManager.ins.amountReveive,0, listDes.Length - 1)];
        goldOld = DataManager.ins.gameSave.gold;
        if (SceneManager.ins.obj_Gold != null) SceneManager.ins.obj_Gold.SetActive(true);
        soundCountdown.PlaySound();//bật tiếng Countdown time
        twn_Countdown.DORestart();//Ảnh loading quay hết một vòng thì tự động gọi hàm CountTime() 1 lần
    }

    //Ảnh loading quay hết một vòng thì tự động gọi hàm CountTime() 1 lần
    public void CountTime() {
        time--;
        if (time >= 0) txt_Time.text = time + "";
        if (time < 0) { //Hết thời gian thì bật Popup thua lên
            if (time == -1 && gameObject.activeSelf) Btn_LoseIt();
        } else {//Nếu chưa hết thời gian thì bật tiếng Countdown time
            soundCountdown.PlaySound();
        }
    }

    #region Button
    public void Btn_Revive() {
        SoundManager.ins.sound_Click.PlaySound();
        GameplayManager.ins.amountRootCur += 2;
        GameplayManager.ins.amountReveive++;
        SceneManager.ins.formHome.ReloadText();
        Close();
    }

    public void Btn_LoseIt() {
        SoundManager.ins.sound_Click.PlaySound();
        SceneManager.ins.ShowPopup_EndGame(false);
        Close();
    }
    #endregion
}


﻿using DG.Tweening;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using TMPro;

public class Form_Loading : FormBase {
    private float speedLoading = 1f;
    public float fillAmountProcess = 0;
    public TextMeshProUGUI txt_Version;
    public int idSkinFull_1;
    public int idSkinFull_2;
    [Header("--------------- RemoteConfig True --------")]
    public int idSkinBody_1;
    public int idSkinBody_2;
    public Image img_ProccessLoading;
    public GameObject obj_ProccessLoading;
    public DOTweenAnimation twn_Logo;

    //public GameObject obj_IconAds_Char1;
    //public GameObject obj_IconAds_Char2;
    //public GameObject obj_LoadingAds_Char1;
    //public GameObject obj_LoadingAds_Char2;
    public GameObject obj_BtnAds_Char1;
    public GameObject obj_BtnAds_Char2;

    [Header("--------------- Select Skin --------")]
    public int idCharSelect = 0;
    public bool isSkin2Unlock = false;
    public bool isSkin3Unlock = false;
    public GameObject obj_Char1;
    public GameObject obj_Char2;
    public GameObject obj_Char3;
    public GameObject obj_SelectChar1;
    public GameObject obj_SelectChar2;
    public GameObject obj_SelectChar3;

    public DOTweenAnimation twn_SelectCharacter;
    private void Awake() {
        fillAmountProcess =0;
        img_ProccessLoading.fillAmount = fillAmountProcess;
        txt_Version.text ="v" + Application.version;
    }

    public override void Show() {
        base.Show();
    }



    private void Update() {
        if (GameManager.ins.isFormLoading_Proccess) {
            // if(img_ProccessLoading.rectTransform.sizeDelta.x < 773) img_ProccessLoading.rectTransform.sizeDelta = new Vector2(img_ProccessLoading.rectTransform.sizeDelta.x + Time.deltaTime/5f *773, img_ProccessLoading.rectTransform.sizeDelta.y);
            fillAmountProcess += speedLoading * Time.deltaTime;
            img_ProccessLoading.fillAmount = fillAmountProcess;
        }
    }

    /*IEnumerator WaitVideoAds() {
        yield return new WaitUntil(() => (MaxManager.ins.isRewardedVideoAvailable()));
        obj_IconAds_Char1.SetActive(true);
        obj_IconAds_Char2.SetActive(true);
        obj_LoadingAds_Char1.SetActive(false);
        obj_LoadingAds_Char2.SetActive(false);
    }*/
}
﻿using System;
using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;
using UnityEngine.UI;
using DG.Tweening;

public class Popup_Settings : PopupBase
{
    [Header("-----------Settings--------")]
    public Sprite on;
    public Sprite off;
    public Button btnMusic;
    public Button btnSound;

    public GameObject obj_BtnHome;

    public override void Show()
    {
        base.Show();

        if (GameplayManager.ins.isStartGame) { 
            obj_BtnHome.SetActive(true);
        } else {
            obj_BtnHome.SetActive(false);
        }
        
        btnSound.GetComponent<Image>().sprite = DataManager.ins.gameSave.volumeSound > 0 ? on : off;
        btnMusic.GetComponent<Image>().sprite = DataManager.ins.gameSave.volumeMusic > 0 ? on : off;
    }

    #region BUTTON
    public void BtnReloadMusic()
    {
        SoundManager.ins.sound_Click.PlaySound();
        if (DataManager.ins.gameSave.volumeMusic > 0)
        {
            DataManager.ins.gameSave.volumeMusic = 0;
            btnMusic.image.sprite = off;
        }
        else
        {
            DataManager.ins.gameSave.volumeMusic = 80;
            btnMusic.image.sprite = on;
        }
        DataManager.ins.SaveGame();
        if (SoundManager.ins != null)
            SoundManager.ins.ReloadMusic();
    }


    public void BtnReloadSound()
    {
        SoundManager.ins.sound_Click.PlaySound();
        if (DataManager.ins.gameSave.volumeSound > 0)
        {
            DataManager.ins.gameSave.volumeSound = 0;
            btnSound.image.sprite = off;
        }
        else
        {
            DataManager.ins.gameSave.volumeSound = 80;
            btnSound.image.sprite = on;
        }
        DataManager.ins.SaveGame();
    }


    public void BtnClose()
    {
        SoundManager.ins.sound_Click.PlaySound();
        if(SceneManager.ins.formGameplay != null) SceneManager.ins.isPause = SceneManager.ins.isPauseSound = false;
            Close();
        DataManager.ins.SaveGame();
    }

    public void BtnHome()
    {
        SoundManager.ins.sound_Click.PlaySound();
        if (SceneManager.ins.formGameplay != null) SceneManager.ins.isPause = SceneManager.ins.isPauseSound = false;
            SceneManager.ins.ChangeForm(FormUI.Form_Home.ToString(), 0);
    }
    #endregion
    #region btn new
    //[Header("------NEW UI------")]
    //public Button btn_sound;
    //public Button btn_vibra;
    //public Button btn_music;
    public void Btn_Sound()
    {
        SoundManager.ins.sound_Click.PlaySound();
        if (DataManager.ins.gameSave.volumeSound > 0)
        {
            DataManager.ins.gameSave.volumeSound = 0;
            //btn_sound.image.sprite = soundOff;
        }
        else
        {
            DataManager.ins.gameSave.volumeSound = 80;
            //btn_sound.image.sprite = soundOn;
        }
        DataManager.ins.SaveGame();
    }
    public void Btn_Music()
    {
        SoundManager.ins.sound_Click.PlaySound();
        if (DataManager.ins.gameSave.volumeMusic > 0)
        {
            DataManager.ins.gameSave.volumeMusic = 0;
            //btn_music.image.sprite = musicOff;
        }
        else
        {
            DataManager.ins.gameSave.volumeMusic = 80;
            //btn_music.image.sprite = musicOn;
        }
        DataManager.ins.SaveGame();
        if (SoundManager.ins != null)
            SoundManager.ins.ReloadMusic();
    }
    #endregion
}

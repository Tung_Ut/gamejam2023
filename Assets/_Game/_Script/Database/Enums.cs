﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Enums : Singleton<Enums>
{
  
}

public enum FormUI {//Ko nên xóa mà nên thêm 
    None,
    Form_Loading,
    Form_Home,
    Form_GamePlay,
}

public enum PopupUI {//Ko nên xóa mà nên thêm 
    None,
    Popup_Rate,
    Popup_EndGame,
    Popup_Revive,
    Popup_Tutorial,
    Popup_Story,
}

public enum Tag {
    None,
    KillZone,
    Character,
    Player,
}

public enum TypeValue {
    None,
    Bool,
    Integer,
    Long,
    Float,
    Double,
    String,
}

public enum TypeCell
{
    None,
    H2O,
    Block,
}


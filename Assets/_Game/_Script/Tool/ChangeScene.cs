﻿#if UNITY_EDITOR
using UnityEditor;
using UnityEditor.SceneManagement;


public class ChangeScene : Editor {

    [MenuItem("Open Scene/Form_Loading #1")]
    public static void OpenLoading()
    {
        OpenScene("Form_Loading");
    }

    [MenuItem("Open Scene/Form_Home #2")]
    public static void OpenForm_Home() {
        OpenScene("Form_Home");
    }

    [MenuItem("Open Scene/Form_GamePlay #3")]
    public static void OpenForm_GamePlay() {
        OpenScene("Form_GamePlay");
    }
    private static void OpenScene (string sceneName) {
		if (EditorSceneManager.SaveCurrentModifiedScenesIfUserWantsTo ()) {
			EditorSceneManager.OpenScene ("Assets/_Game/_Scenes/" + sceneName + ".unity");
		}
	}
}
#endif